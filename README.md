# Payout Sample Code

```
The sample code runs against the payout endpoints, https://snapshot.getswish.pub.tds.tieto.com/cpc-swish/api/v1/payouts/
The properties under conf/ can be overriden in order to change amount, payee alias etc. etc.
```

## Build a runnable jar 
```
mvn clean install
```

## Run the sample program
```
java -jar payout_sample-develop-SNAPSHOT.jar
```

## Print curl command
```
java -jar payout_sample-develop-SNAPSHOT.jar arg
```

## Example Run
```
swish-payout-integration-examples$ java -jar payout_sample-develop-SNAPSHOT.jar
/home/sernhmik/repo/swish-payout-integration-examples
/home/sernhmik/repo/swish-payout-integration-examples/conf/certs/clientCert.p12
using properties...
payout.customer.message message
payout.customer.ssn 196210123235
payout.keystore.password swish
payout.keystore.client.path /conf/certs/clientCert.p12
payout.customer.alias 46768648198
payout.location.url https://snapshot.getswish.pub.tds.tieto.com/cpc-swish/api/v1/payouts/
payout.customer.amount 200
payout.keystore.sign.path /conf/certs/clientSign.p12
Creating payout...
Fetching payout status...
CREATED
DEBITED
DEBITED
DEBITED
PAID
```