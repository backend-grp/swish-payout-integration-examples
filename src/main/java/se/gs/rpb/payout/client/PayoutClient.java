package se.gs.rpb.payout.client;

import org.json.JSONObject;
import se.gs.rpb.payout.service.ConnectionFactory;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;

public class PayoutClient {

    private ConnectionFactory connectionFactory;

    public PayoutClient(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public String createPayout(JSONObject payoutRequest, String url) {
        return executePOST(payoutRequest, url);
    }

    private String executePOST(JSONObject payoutRequest, String url) {
        HttpURLConnection connection = connectionFactory.createConnection(url);

        try (AutoCloseable closeableConnection = connection::disconnect) {
            connection.setRequestMethod("POST");
            connection.addRequestProperty("Accept", "application/json");
            connection.addRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            // Send data
            byte[] data = payoutRequest.toString().getBytes(StandardCharsets.UTF_8);

            try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
                outputStream.write(data);
                outputStream.flush();
                outputStream.close();

                switch (connection.getResponseCode()) {
                    case 200:
                    case 201:
                        return connection.getHeaderField("Location");
                    default:
                        throw new RuntimeException("Something went wrong when trying to create payout. "
                                + connection.getResponseCode());
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public JSONObject getPayoutStatus(String url) {
        int numberOfTries = 20;
        while (numberOfTries > 0) {
            JSONObject response = executeGET(url);
            String status = (String)response.get("status");

            switch(status) {
                case "PAID":
                    System.out.println(status);
                    return response;
                case "ERROR":
                    System.out.println(response.toString());
                    return response;
                default:
                    System.out.println(status);
            }
            try {
                numberOfTries--;
                Thread.sleep(300);
            } catch (Exception e) {
                throw new RuntimeException("Something went wrong when retrieving payout status.", e);
            }
        }
        return null;
    }

    private JSONObject executeGET(String url) {

        HttpURLConnection connection = connectionFactory.createConnection(url);

        try (AutoCloseable closeableConnection = connection::disconnect) {
            connection.setRequestMethod("GET");
            connection.addRequestProperty("Accept", "application/json");

            connection.connect();

            if (connection.getResponseCode() > 299) {
                InputStream in = new BufferedInputStream(connection.getErrorStream());
                String response = readResponse(in);
                throw new RuntimeException("Error when invoking endpoint. " + response);
            } else {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                String response = readResponse(in);
                return new JSONObject(response);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String readResponse(InputStream inputStream) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        StringBuilder result = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }
}