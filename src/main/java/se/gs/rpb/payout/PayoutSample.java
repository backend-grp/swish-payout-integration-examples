package se.gs.rpb.payout;

import org.json.JSONObject;
import se.gs.rpb.payout.client.PayoutClient;
import se.gs.rpb.payout.service.ConnectionFactory;
import se.gs.rpb.payout.service.KeyStoreService;
import se.gs.rpb.payout.util.PropertyLoader;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.Signature;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

public class PayoutSample {

    public static void main(String[] args) {

        PropertyLoader propertyLoader = new PropertyLoader();

        Properties defaultProperties = propertyLoader.loadProperties();

        File file = new File("./");
        Path path = Paths.get(file.getAbsolutePath()).getParent().toAbsolutePath();

        KeyStoreService clientCertService = new KeyStoreService(path.toAbsolutePath() + "/" + defaultProperties.getProperty("payout.keystore.client.path"),
                defaultProperties.getProperty("payout.keystore.password"),
                "1");
        KeyStoreService clientSignCertService = new KeyStoreService(path.toAbsolutePath() + "/" + defaultProperties.getProperty("payout.keystore.sign.path"),
                defaultProperties.getProperty("payout.keystore.password"),
                "1");

        JSONObject payoutRequest = createPayoutRequest(clientCertService, clientSignCertService, defaultProperties);

        if (args.length > 0) {
            printCurl(payoutRequest, defaultProperties);
            System.exit(0);
        }

        printProperties(defaultProperties);
        System.out.println("Creating payout...");
        ConnectionFactory connectionFactory = new ConnectionFactory(clientCertService);
        PayoutClient client = new PayoutClient(connectionFactory);
        String statusLocation = client.createPayout(payoutRequest, defaultProperties.getProperty("payout.location.url"));

        System.out.println("Fetching payout status...");
        client.getPayoutStatus(statusLocation);
    }

    private static JSONObject createPayoutRequest(KeyStoreService clientCertService,
                                                  KeyStoreService clientSignCertService,
                                                  Properties properties) {

        JSONObject payload = new JSONObject();
        payload.put("payerPaymentReference","orderId");
        String uuid = UUID.randomUUID().toString();
        payload.put("payoutInstructionUUID", uuid.replace("-", "").toUpperCase());
        String payerAlias = clientCertService.getCertificateCommonName();
        payload.put("payerAlias", payerAlias);
        payload.put("payeeAlias", properties.getProperty("payout.customer.alias"));
        payload.put("payeeSSN", properties.getProperty("payout.customer.ssn"));
        payload.put("amount", properties.getProperty("payout.customer.amount"));
        payload.put("currency", "SEK");
        payload.put("payoutType", "PAYOUT");
        payload.put("message", properties.getProperty("payout.customer.message"));

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date parse = null;
        try {
            parse = dateFormat.parse("2021-06-03T15:45:00.000+01:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String instructionDate = dateFormat.format(parse);
        payload.put("instructionDate", instructionDate);

        String serialNumber = clientSignCertService.getCertificateSerialNumber();
        payload.put("signingCertificateSerialNumber", serialNumber);

        String signature = createSignature(payload.toString(), clientSignCertService.getPrivateKey());

        JSONObject payoutRequest = new JSONObject();
        payoutRequest.put("payload", payload);
        payoutRequest.put("callbackUrl", "https://mystore/payments/swish/callback/");
        payoutRequest.put("signature", signature);

        return payoutRequest;
    }

    private static String createSignature(String stringToBeSigned, PrivateKey privateKey) {
        try {
            byte[] hashString = hashString(stringToBeSigned);

            Signature sign = Signature.getInstance("SHA512withRSA");
            sign.initSign(privateKey);
            sign.update(hashString);

            byte[] signature = sign.sign();
            return Base64.getEncoder().encodeToString(signature);
        } catch (Exception e) {
            throw new RuntimeException("Could not create signed payout request.", e);
        }
    }

    private static byte[] hashString(String stringToBeHashed) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            return digest.digest(stringToBeHashed.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void printProperties(Properties properties) {
        System.out.println("using properties...");
        for (Map.Entry<Object, Object> e : properties.entrySet()) {
           System.out.println(e.getKey() + " " +e.getValue());
        }
    }

    private static void printCurl(JSONObject payoutRequest, Properties defaultProperties) {
        StringBuilder sb = new StringBuilder();
        sb.append("curl -v --request POST ");
        sb.append(defaultProperties.getProperty("payout.location.url"));
        sb.append(" --cert-type P12");
        sb.append(" --cert " + defaultProperties.getProperty("payout.keystore.client.path") + ":" + defaultProperties.getProperty("payout.keystore.password"));
        sb.append(" --header \"Content-Type: application/json\" --data '");
        sb.append(payoutRequest.toString(3));
        sb.append("'");
        System.out.println(sb.toString());
    }
}