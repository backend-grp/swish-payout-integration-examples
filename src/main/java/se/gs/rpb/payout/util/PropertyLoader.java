package se.gs.rpb.payout.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

public class PropertyLoader {
    public Properties loadProperties(String propertiesPath) {
        Properties properties = new Properties();
        try {
            //load external properties file
            File file = new File(propertiesPath.trim());
            properties.load(new FileInputStream(file));
        } catch (Exception e) {
                throw new RuntimeException("Could not load properties.", e);
        }
        return properties;
    }

    public Properties loadProperties() {
        Properties properties = new Properties();
        try {

            File file = new File("./");

            Path path = Paths.get(file.getAbsolutePath()).getParent().toAbsolutePath();
            System.out.println(path);
            InputStream inputStream = Files.newInputStream( Paths.get(path.toAbsolutePath() + "/conf/application.properties"));
            properties.load(inputStream);

        } catch (Exception e) {
            throw new RuntimeException("Could not load properties.", e);
        }
        return properties;
    }

    public Properties overrideProperties(Properties properties, Properties newProperties) {

        for (Map.Entry<Object, Object> e : properties.entrySet()) {
            for (Object key : newProperties.keySet()) {
                if (((String)e.getKey()).contains((String)key)) {
                    e.setValue(newProperties.get(key));
                }
            }
        }
        return properties;
    }
}
