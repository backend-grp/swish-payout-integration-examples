package se.gs.rpb.payout.service;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;

public class ConnectionFactory {

    private final KeyStoreService clientCertService;
    private static final int CONN_READ_TIMEOUT = 15 * 1000;
    private static final int CONN_TIMEOUT = 3 * 1000;
    private SSLContext sslContext;

    public ConnectionFactory(KeyStoreService clientCertService) {
        this.clientCertService = clientCertService;
        this.sslContext = createSSLContext();
    }

    private SSLContext createSSLContext() {
        try {
            KeyStore keyStore = clientCertService.getKeyStore();

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, clientCertService.getPassword().toCharArray());
            KeyManager[] kms = kmf.getKeyManagers();

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init((KeyStore) null); //Use javas own trustStore.
            TrustManager[] tms = tmf.getTrustManagers();

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kms, tms, new SecureRandom());
            return sslContext;
        } catch (Exception e) {
            throw new RuntimeException("Could not create ssl context. ", e);
        }
    }

    public HttpURLConnection createConnection(String url) {
        String[] split = url.split(":");

        switch(split[0].toUpperCase()) {
            case "HTTP":
                return createHttpURLConnection(url);
            case "HTTPS":
                return createHttpsURLConnection(url);
            default:
                throw new RuntimeException("Could not create connection, invalid url.");
        }
    }

    private HttpsURLConnection createHttpsURLConnection(String url) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
            connection.setSSLSocketFactory(sslContext.getSocketFactory());
            connection.setConnectTimeout(CONN_TIMEOUT);
            connection.setReadTimeout(CONN_READ_TIMEOUT);

            return connection;
        } catch (Exception e) {
            throw new RuntimeException("Could not create connection to " + url, e);
        }
    }

    private HttpURLConnection createHttpURLConnection(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(CONN_TIMEOUT);
            connection.setReadTimeout(CONN_READ_TIMEOUT);

            return connection;
        } catch (Exception e) {
            throw new RuntimeException("Could not create connection to " + url, e);
        }
    }
}