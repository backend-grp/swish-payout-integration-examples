package se.gs.rpb.payout.service;

import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Iterator;

public class KeyStoreService {
    private final String pathToKeyStore;
    private final String password;
    private final String storeType = "PKCS12";
    private final String alias;
    private final KeyStore keyStore;

    public KeyStoreService(String path, String password, String alias) {
        this.pathToKeyStore = path;
        this.password = password;
        this.alias = alias;
        this.keyStore = loadKeyStore();
    }

    private KeyStore loadKeyStore() {
        try {
            KeyStore keyStore = KeyStore.getInstance(storeType);
            try (InputStream kStoreStream = Files.newInputStream(Paths.get(pathToKeyStore))) {
                keyStore.load(kStoreStream, password.toCharArray());
            }
            return keyStore;
        } catch (Exception e) {
            throw new RuntimeException("Failed to load KeyStore.", e);
        }
    }

    public KeyStore getKeyStore() {
        return keyStore;
    }

    public PrivateKey getPrivateKey() {
        try {
            return (PrivateKey) keyStore.getKey(alias, password.toCharArray());
        } catch (Exception e) {
            throw new RuntimeException("Failed to load private key.", e);
        }
    }

    public Certificate getCertificate() {
        try {
            return keyStore.getCertificate(alias);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load private key.", e);
        }
    }

    public String getCertificateSerialNumber() {
        X509Certificate certificate = (X509Certificate)getCertificate();
        BigInteger serialNumber = certificate.getSerialNumber();
        return serialNumber.toString(16);
    }

    public String getCertificateAsString() {
        Certificate certificate = getCertificate();

        try {
            return Base64.getEncoder().encodeToString(certificate.getEncoded());
        } catch (Exception e) {
            throw new RuntimeException("Could not encode certificate");
        }
    }

    public String getCertificateCommonName() {
        X509Certificate certificate = (X509Certificate)getCertificate();

        try {
            Principal p = certificate.getSubjectDN();
            LdapName ldapDN = new LdapName(p.getName());
            Iterator var5 = ldapDN.getRdns().iterator();

            while (var5.hasNext()) {
                Rdn rdn = (Rdn) var5.next();
                if ("CN".equalsIgnoreCase(rdn.getType())) {
                    return (String) rdn.getValue();
                }
            }
            throw new RuntimeException("CommonName(payerAlias) couldn't be found in client certificate.");
        } catch (Exception e) {
            throw new RuntimeException("CommonName(payerAlias) couldn't be found in client certificate.", e);
        }
    }

    public String getPassword() {
        return password;
    }
}
