package se.gs.rpb.payout.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.net.HttpURLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ConnectionFactoryTest {

    private final String httpsURL = "https://snapshot.getswish.pub.tds.tieto.com/cpc-swish/api/v1/payouts/";
    private final String httpURL = "http://someinternalserver:8080/cpc-swish/api/v1/payouts/";
    private final String certPath = "conf/certs/Swish_Merchant_TestCertificate_1234679304.p12";
    private final String password = "swish";

    private ConnectionFactory connectionFactory;
    private KeyStoreService keyStoreService;
    @Before
    public void init() {

        File file = new File("./");
        Path path = Paths.get(file.getAbsolutePath()).getParent().toAbsolutePath();

        KeyStoreService clientCertService = new KeyStoreService(path + "/" + certPath, password, "1");
        this.connectionFactory = new ConnectionFactory(clientCertService);
    }

    @Test
    public void createConnectionFactoryTest() {
        File file = new File("./");
        Path path = Paths.get(file.getAbsolutePath()).getParent().toAbsolutePath();
        KeyStoreService clientCertService = spy(new KeyStoreService(path + "/" + certPath, password, "1"));
        ConnectionFactory connectionFactory = new ConnectionFactory(clientCertService);

        verify(clientCertService).getKeyStore();

        SSLContext sslContext = (SSLContext) Whitebox.getInternalState(connectionFactory, "sslContext");

        assertNotNull(sslContext);
        assertEquals("TLS", sslContext.getProtocol());
    }

    @Test
    public void createConnectionHttpsTest() {
        HttpURLConnection connection = connectionFactory.createConnection(httpsURL);

        SSLContext sslContext = (SSLContext) Whitebox.getInternalState(connectionFactory, "sslContext");
        assertNotNull(sslContext);
        assertEquals(3*1000, connection.getConnectTimeout());
        assertEquals(15*1000, connection.getReadTimeout());
    }

    @Test
    public void createHttpURLConnectionHttp() {
        HttpURLConnection connection = connectionFactory.createConnection(httpURL);

        assertEquals(3*1000, connection.getConnectTimeout());
        assertEquals(15*1000, connection.getReadTimeout());
    }

    @Test(expected = RuntimeException.class)
    public void createConnectionHttps_invalidUrlTest() {
        connectionFactory.createConnection("file://someserver/");
    }
}