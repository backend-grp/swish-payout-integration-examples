package se.gs.rpb.payout.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import se.gs.rpb.payout.util.PropertyLoader;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;

import static junit.framework.TestCase.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class PropertyLoaderTest {

    @Before
    public void init() {
        System.clearProperty("propertiesPath");
    }

    @Test
    public void loadDefaultPropertiesTest() {
        PropertyLoader loader = new PropertyLoader();
        Properties properties = loader.loadProperties();

        Enumeration<?> names = properties.propertyNames();
        ArrayList<?> props = Collections.list(names);

        assertTrue(!props.isEmpty());
    }

    @Test
    public void loadExternalPropertiesTest() {
        PropertyLoader loader = new PropertyLoader();
        File file = new File("./");

        Path path = Paths.get(file.getAbsolutePath()).getParent().toAbsolutePath();
        Properties properties = loader.loadProperties(path.toAbsolutePath() + "/conf/application.properties");

        Enumeration<?> names = properties.propertyNames();
        ArrayList<?> props = Collections.list(names);

        assertTrue(!props.isEmpty());
    }

    @Test(expected = RuntimeException.class)
    public void loadProperties_fileNotFoundTest() {
        PropertyLoader loader = new PropertyLoader();
        loader.loadProperties("wrongDir/application.properties");
    }
}