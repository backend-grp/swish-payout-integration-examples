package se.gs.rpb.payout.service;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.KeyStoreSpi;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.mockito.internal.util.reflection.Whitebox;

@RunWith(MockitoJUnitRunner.class)
public class KeyStoreServiceTest {

    private static String clientCertP12 = "";
    private static final String password = "swish";

    private KeyStoreService keyStoreService;

    @Before
    public void init() {
        File file = new File("./");
        Path path = Paths.get(file.getAbsolutePath()).getParent().toAbsolutePath();
        clientCertP12 = path.toAbsolutePath() + "/conf/certs/Swish_Merchant_TestCertificate_1234679304.p12";

        keyStoreService = new KeyStoreService(clientCertP12, password, "1");
    }

    @Test
    public void getCertificateTest() {
        Certificate certificate = keyStoreService.getCertificate();
        assertNotNull(certificate);
    }

    @Test(expected = RuntimeException.class)
    public void getCertificate_exceptionTest() throws Exception {
        KeyStoreSpi keyStoreSpi = mock(KeyStoreSpi.class);
        KeyStore keyStore = new KeyStore(keyStoreSpi, null, "test"){ };
        keyStore.load(null);

        Whitebox.setInternalState(keyStoreService, "keyStore", keyStore);
        when(keyStore.getCertificate(anyString())).thenThrow(new RuntimeException());

        keyStoreService.getCertificate();
    }

    @Test
    public void getCertificateSerialNumberTest() {
        String serialNumber = keyStoreService.getCertificateSerialNumber();
        assertNotNull(serialNumber);
    }

    @Test
    public void getCertificateCommonNameTest() {
        String name = keyStoreService.getCertificateCommonName();
        assertNotNull(name);
        assertTrue(name.length() == 10);
        assertTrue(name.matches("\\d+"));
    }

    @Test(expected = RuntimeException.class)
    public void getCertificateCommonName_notFoundTest() throws Exception {
        KeyStoreSpi keyStoreSpi = mock(KeyStoreSpi.class);
        KeyStore keyStore = new KeyStore(keyStoreSpi, null, "test"){ };
        keyStore.load(null);

        Whitebox.setInternalState(keyStoreService, "keyStore", keyStore);

        X509Certificate cert = mock(X509Certificate.class);
        when(keyStore.getCertificate(anyString())).thenReturn(cert);
        when(cert.getSubjectDN()).thenReturn(new Principal() {
            @Override
            public String getName() {
                return "CNN=1232329369, O=5564604812, C=SE";
            }
        });

        keyStoreService.getCertificateCommonName();
    }

    @Test(expected = RuntimeException.class)
    public void getCertificateCommonName_exceptionTest() throws Exception {
        KeyStoreSpi keyStoreSpi = mock(KeyStoreSpi.class);
        KeyStore keyStore = new KeyStore(keyStoreSpi, null, "test"){ };
        keyStore.load(null);

        Whitebox.setInternalState(keyStoreService, "keyStore", keyStore);

        X509Certificate cert = mock(X509Certificate.class);
        when(keyStore.getCertificate(anyString())).thenReturn(cert);
        when(cert.getSubjectDN()).thenThrow(new RuntimeException());

        keyStoreService.getCertificateCommonName();
    }

    @Test
    public void getKeyStoreTest() {
        KeyStore keyStore = keyStoreService.getKeyStore();
        assertNotNull(keyStore);
    }

    @Test
    public void getPrivateKeyTest() {
        PrivateKey privateKey = keyStoreService.getPrivateKey();
        assertNotNull(privateKey);
    }

    @Test(expected = RuntimeException.class)
    public void getPrivateKey_exceptionTest() throws Exception {
        KeyStoreSpi keyStoreSpi = mock(KeyStoreSpi.class);
        KeyStore keyStore = new KeyStore(keyStoreSpi, null, "test"){ };
        keyStore.load(null);

        Whitebox.setInternalState(keyStoreService, "keyStore", keyStore);
        when(keyStore.getKey(anyString(), any(char[].class))).thenThrow(new RuntimeException());

        keyStoreService.getPrivateKey();
    }

    @Test
    public void getPasswordTest() {
        String password = keyStoreService.getPassword();
        assertEquals(this.password, password);
    }
}