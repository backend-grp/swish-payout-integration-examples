package se.gs.rpb.payout.client;

import jdk.nashorn.internal.ir.RuntimeNode;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import se.gs.rpb.payout.service.ConnectionFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.UUID;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class PayoutClientTest {

    private final String httpsURL = "https://snapshot.getswish.pub.tds.tieto.com/cpc-swish/api/v1/payouts/";

    @Before
    public void init() {
    }

    @Test
    public void createPayoutTest() throws IOException {
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        PayoutClient client = new PayoutClient(connectionFactory);
        HttpURLConnection connection = mock(HttpURLConnection.class);

        when(connectionFactory.createConnection(anyString())).thenReturn(connection);
        when(connection.getOutputStream()).thenReturn(mock(OutputStream.class));
        when(connection.getResponseCode()).thenReturn(201);
        when(connection.getHeaderField("Location")).thenReturn(httpsURL + UUID.randomUUID().toString().toUpperCase());

        String location = client.createPayout(new JSONObject(), httpsURL);
        assertTrue(location.matches("^(https|http)://[^\\s]+"));
        verify(connectionFactory, times(1)).createConnection(anyString());
        verify(connection, times(1)).setRequestMethod("POST");
        verify(connection, times(1)).disconnect();
    }

    @Test(expected = RuntimeException.class)
    public void createPayout_errorResponseTest() throws IOException {
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        PayoutClient client = new PayoutClient(connectionFactory);
        HttpURLConnection connection = mock(HttpURLConnection.class);

        when(connectionFactory.createConnection(anyString())).thenReturn(connection);
        when(connection.getOutputStream()).thenReturn(mock(OutputStream.class));
        when(connection.getResponseCode()).thenReturn(401);

        try {
            client.createPayout(new JSONObject(), httpsURL + UUID.randomUUID().toString().toUpperCase());
        } catch (Exception e) {
            verify(connectionFactory, times(1)).createConnection(anyString());
            verify(connection, times(1)).setRequestMethod("POST");
            verify(connection, times(1)).disconnect();
            throw e;
        }
    }

    @Test
    public void getPayoutStatusTest() throws IOException {
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        PayoutClient client = new PayoutClient(connectionFactory);
        HttpURLConnection connection = mock(HttpURLConnection.class);

        when(connectionFactory.createConnection(anyString())).thenReturn(connection);
        when(connection.getInputStream()).thenAnswer(new Answer() {
            private int answearedTimes = 0;

            @Override
            public ByteArrayInputStream answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (answearedTimes++ < 1)
                    return new ByteArrayInputStream("{status:'CREATED'}".getBytes());

                return new ByteArrayInputStream("{status:'PAID'}".getBytes());
            }
        });
        
        JSONObject response = client.getPayoutStatus(httpsURL + UUID.randomUUID().toString().toUpperCase());

        assertEquals("PAID", response.getString("status"));
        verify(connectionFactory, times(2)).createConnection(anyString());
        verify(connection, times(2)).setRequestMethod("GET");
        verify(connection, times(2)).disconnect();
    }

    @Test
    public void getPayoutStatus_errorTest() throws IOException {
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        PayoutClient client = new PayoutClient(connectionFactory);
        HttpURLConnection connection = mock(HttpURLConnection.class);

        when(connectionFactory.createConnection(anyString())).thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(201);
        when(connection.getInputStream()).thenAnswer(new Answer() {
            @Override
            public ByteArrayInputStream answer(InvocationOnMock invocationOnMock) throws Throwable {
                return new ByteArrayInputStream("{status:'ERROR'}".getBytes());
            }
        });

        JSONObject response = client.getPayoutStatus(httpsURL + UUID.randomUUID().toString().toUpperCase());

        assertEquals("ERROR", response.getString("status"));
        verify(connectionFactory, times(1)).createConnection(anyString());
        verify(connection, times(1)).setRequestMethod("GET");
        verify(connection, times(1)).disconnect();
    }

    @Test(expected = RuntimeException.class)
    public void getPayoutStatus_notAuthTest() throws IOException {
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        PayoutClient client = new PayoutClient(connectionFactory);
        HttpURLConnection connection = mock(HttpURLConnection.class);

        when(connectionFactory.createConnection(anyString())).thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(401);
        when(connection.getErrorStream()).thenAnswer(new Answer() {
            @Override
            public ByteArrayInputStream answer(InvocationOnMock invocationOnMock) throws Throwable {
                return new ByteArrayInputStream("{errorCode:'PA01'}".getBytes());
            }
        });

        try {
            JSONObject response = client.getPayoutStatus(httpsURL + UUID.randomUUID().toString().toUpperCase());
        } catch (Exception e) {
            verify(connectionFactory, times(1)).createConnection(anyString());
            verify(connection, times(1)).setRequestMethod("GET");
            verify(connection, times(1)).disconnect();
            throw e;
        }
    }

    @Test(expected = RuntimeException.class)
    public void getPayoutStatus_unexpectedErrorTest() {
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        PayoutClient client = new PayoutClient(connectionFactory);
        HttpURLConnection connection = mock(HttpURLConnection.class);

        when(connectionFactory.createConnection(anyString())).thenThrow(new RuntimeException("Unexpected"));

        client.getPayoutStatus(httpsURL + UUID.randomUUID().toString().toUpperCase());
    }
}
